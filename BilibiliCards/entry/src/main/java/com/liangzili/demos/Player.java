package com.liangzili.demos;

import com.liangzili.demos.slice.PlayerSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class Player extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(PlayerSlice.class.getName());
    }
}
